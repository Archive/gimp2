		       Concurrency maintenance


There's obviously need for some sort of concurrency control. Otherwise 
modules could operate on the same resources simultaneously and there
would be a big mess (as occasionally happens currently).

However, The Gimp is an image manipulation program, not a DBMS. There
is no real need for efficiency on this area, as resource conflicts are 
likely to be uncommon (under interactive use in any case).

So it seems like a conservative 2-phase two-mode locking mechanism is
going to suit our needs. That is, if the process wants to allocate
further locks once it has started, it has to release all previous
locks first, possibly reclaiming them immediately. Locks can be
downgraded at will. This requires that multiple locks can be acquired
atomically, which in practice requires that there be a single
authority that manages locks. This would be the "core".

More complex and efficient solutions can be devised, too, if they are
deemed necessary. Getting rid of deadlocks is only an issue in
non-interactive processing, as it's perfectly acceptable for a module
in interactive mode to simply fail and return if a resource was not
available.

Then there is the question of what resources need locking, and what is 
an appropriate granularity. Since efficiency is not an issue here, but 
convenience to the user is, a relatively fine granularity is probably
best. This would mean that one could lock

* Single gradients
* Single palette/colormap entries
* Individual tiles in a drawable

among other things. A fine granularity allows two modules two work on
different areas of an image at the same time, which is probably nice
if you have a parallel system and heavy manipulation to do.

The real benefit with being able to operate multiple modules on a single image
simultaneously is that on an SMP machine, a filter operation on an image can
be divided in many parts, and a separate process can do a part of the image at
the same time. This should be a pretty clean way of distributing the filter
operation to multiple processors at the same time.

Or, since modules will be distributed, a single image operation can be handled
by multiple _machines_ simultaneously.

